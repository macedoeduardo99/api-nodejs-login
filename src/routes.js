const express = require('express');
const routes = express.Router();
const authController = require('./controllers/authController');
const projectController = require('./controllers/projectController');

routes.post('/register', authController.register);
routes.post('/authenticate', authController.authenticate);
routes.post('/users', )
module.exports = routes;

